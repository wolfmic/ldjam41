// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "VehicleAdvGameMode.h"
#include "VehicleAdvPawn.h"
#include "VehicleAdvHud.h"

AVehicleAdvGameMode::AVehicleAdvGameMode()
{
	DefaultPawnClass = AVehicleAdvPawn::StaticClass();
	HUDClass = AVehicleAdvHud::StaticClass();
}
