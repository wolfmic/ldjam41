

#include "Vroum.h"


// Sets default values
AVroum::AVroum()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));

	//SetRootComponent(scene);
	c = CreateDefaultSubobject<UBoxComponent>(TEXT("Bruh"));
	non = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Oui"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("La camera"));
	
	speed = 0;
	straff = 0;
}

FVector4 AVroum::ToSplineLocation() {
	FVector4 worldSplineLocation = TrackSpline->SplineComponent->GetWorldLocationAtDistanceAlongSpline(distance);
	FVector4 worldSplineLocationAnticipated = TrackSpline->SplineComponent->GetWorldLocationAtDistanceAlongSpline(distance);
	FVector f = TrackSpline->SplineComponent->FindDirectionClosestToWorldLocation(worldSplineLocationAnticipated, ESplineCoordinateSpace::World);
	FVector u = TrackSpline->SplineComponent->FindUpVectorClosestToWorldLocation(worldSplineLocationAnticipated, ESplineCoordinateSpace::World);
	FVector r = TrackSpline->SplineComponent->FindRightVectorClosestToWorldLocation(worldSplineLocationAnticipated, ESplineCoordinateSpace::World);

	FVector4 offset = -r * 0 + u * 150 + r * straff + u * sin(distance / 100) * 30;
	SetActorRotation(TrackSpline->SplineComponent->GetWorldRotationAtDistanceAlongSpline(distance) + FRotator(rotPower * 8, rotPower*5, 0) + FRotator(0, -90, 0));
	return worldSplineLocation + offset;
}

// Called when the game starts or when spawned
void AVroum::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AVroum::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (TrackSpline == nullptr)
		return ;
	distance += speed;
	SetActorLocation(ToSplineLocation());
	speed /= 1.4;
	rotPower /= 1.02;
	straff /= 1.2;
}

void AVroum::MoveForward(float value) {
		speed = 10 * value;
}

void AVroum::MoveRight(float value) {
		rotPower += 2 * value * speed/100;
	straff += rotPower * 10;
}


// Called to bind functionality to input
void AVroum::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	PlayerInputComponent->BindAxis("MoveForward", this, &AVroum::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AVroum::MoveRight);
	PlayerInputComponent->BindAxis("LookUp");
	PlayerInputComponent->BindAxis("LookRight");
}

