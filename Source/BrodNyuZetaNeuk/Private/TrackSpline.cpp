#include "TrackSpline.h"

ATrackSpline::ATrackSpline() {
	PrimaryActorTick.bCanEverTick = true;

	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("spline"));
	SplineComponent->SetupAttachment(RootComponent);
}

void ATrackSpline::OnConstruction(const FTransform& transform) {
	Super::OnConstruction(transform);

	numberOfPoints = SplineComponent->GetNumberOfSplinePoints();
	int count = SplineDatas.Num();

	while (count != numberOfPoints) {
		if (count > numberOfPoints) {
			SplineDatas.RemoveAt(count - 1);
			count--;
		} else if (count < numberOfPoints) {
			SplineDatas.Add(FSplineData());
			count++;
		}
	}

	for (int i = 0; i <= numberOfPoints - 2; i++) {
		NewSplineElement(i, Mesh);
	}

	RegisterAllComponents();
}

void ATrackSpline::BeginPlay() {
	Super::BeginPlay();
}

void ATrackSpline::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void ATrackSpline::NewSplineElement(int index, UStaticMesh* elementMesh) {
	FVector startLocation, startTangent, endLocation, endTangent;
	auto startScale = FVector2D(SplineDatas[index].width, SplineDatas[index].thickness);;
	auto endScale = FVector2D(SplineDatas[index + 1].width, SplineDatas[index + 1].thickness);

	SplineComponent->GetLocationAndTangentAtSplinePoint(index, startLocation, startTangent, ESplineCoordinateSpace::Local);
	SplineComponent->GetLocationAndTangentAtSplinePoint(index + 1, endLocation, endTangent, ESplineCoordinateSpace::Local);

	auto splineMesh = NewObject<USplineMeshComponent>(this);
	splineMesh->CreationMethod = EComponentCreationMethod::UserConstructionScript;
	splineMesh->SetupAttachment(RootComponent);
	splineMesh->SetMobility(EComponentMobility::Movable);
	splineMesh->SetStaticMesh(elementMesh);
	splineMesh->SetStartAndEnd(startLocation, startTangent, endLocation, endTangent);
	splineMesh->SetStartRoll(SplineDatas[index].bank);
	splineMesh->SetEndRoll(SplineDatas[index + 1].bank);
	splineMesh->SetStartScale(startScale);
	splineMesh->SetEndScale(endScale);
	splineMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}