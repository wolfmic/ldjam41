

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "EngineMinimal.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "TrackSpline.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Vroum.generated.h"

UCLASS()
class BRODNYUZETANEUK_API AVroum : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVroum();

	/** Handle pressing forwards */
	void MoveForward(float Val);

	/** Handle pressing right */
	void MoveRight(float Val);
	/** Handle handbrake pressed */

	UPROPERTY(Category = Scene, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent* scene;

	UPROPERTY(Category = oui, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* c;
	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* non;

	/** Camera component that will be our viewpoint */
	UPROPERTY(Category = oui, VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;


	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float accelerationSpeed;

	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float rotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly) class ATrackSpline*		TrackSpline;

	FVector4 ToSplineLocation();
	float distance;
	float straff;
	float rotPower;
	float speed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
