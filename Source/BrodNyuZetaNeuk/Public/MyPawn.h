#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"

UCLASS()
class BRODNYUZETANEUK_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* mesh;

	UPROPERTY(Category = Mesh, VisibleAnywhere, BlueprintReadWrite)
		bool hellDrifuttoKiddo;

	// Sets default values for this pawn's properties
	AMyPawn();

	/** Handle pressing forwards */
	void MoveForward(float Val);

	/** Handle pressing right */
	void MoveRight(float Val);
	/** Handle handbrake pressed */
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
